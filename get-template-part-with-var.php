<?php
/**
 * Plugin Name: Get Template Part With Var
 * Plugin URI: http:addactiondan.me
 * Description: Functions to allow sending arbitray PHP vars as an array to get_template_part, also in
 * response to https://core.trac.wordpress.org/ticket/21676
 * Author: Dan Beil
 * Version: 0.1-super-beta
 * Author URI: http:addactiondan.me
 * Text Domain: get-template-part-with-var
 *
 * How to use:
 * This plugin provides the following two functions which may be useful as a replacment to
 * get_template_part and get_post_meta.
 *
 * With this plugin active you will have access to two additional functions, ddb_get_template_part_w_var() and
 * ddb_get_template_vars(). This first function ddb_get_template_part_w_var() requires two arguments, the first
 * being a string as accepted by WP Core's get_template_part, the second is an array where the _one_ key
 * will server as the way to access the value of this array later, the value can be anything accepted as an array value.
 *
 * i.e.
 * ddb_get_template_part_w_var( 'template-parts/my-cool-template', array( 'my_key' => 'I need this string in this template part' ) );
 *
 * or
 *
 * ddb_get_template_part_w_var( 'template-parts/my-cool-template', array( 'my_key' => $an_array_of_meta_data ) );
 *
 * @package get-template-part-with-var
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * This function allows for passing arbitray variable to a template part
 * via a temporary global variable
 *
 * @param  string       $template template location as expected by get_template_part().
 * @param  array $var an arbitray sting or variable to be accessible by the template part. i.e. array( 'my_var' => 'string' ) or array( 'my_var' => $array )
 * @uses  get_template_part()
 */
function ddb_get_template_part_w_var( $template = '', $var = '' ) {

	/**
	 * Oops, we didnt get an array
	 *
	 */
	if ( ! is_array( $var ) || empty( $template ) ) {
		return;
	}

	/**
	 * Setting our global array
	 */
	if ( ! array_key_exists( 'ddb_template_part_vars', $GLOBALS ) ) {
		$GLOBALS['ddb_template_part_vars'] = array();
	}

	/**
	 * Getting access to our global
	 */
	global $ddb_template_part_vars;

	/**
	 * Getting our key
	 */
	$key = key( $var );

	/**
	 * Moving our var array to the global scope
	 */
	$ddb_template_part_vars[ $template ][ $key ] = $var[ $key ];

	/**
	 * WordPress core get_template_part
	 *
	 * @todo  account or double check functionality for $name, https://developer.wordpress.org/reference/functions/get_template_part/ - from jorbin
	 */
	get_template_part( $template );
}

/**
 * This function retrieves the passed variable, similar to get_post_meta,
 * passing the correct key ( i.e. the key set using ddb_get_template_part_w_var()
 * will result in retrieving the passed variable from ddb_get_template_part_w_var.
 *
 * Usage:
 * in a called template part:
 * $vars = ddb_get_template_vars( 'my_key' ) will result in retrieving the variable passed
 * using ddb_get_template_part_w_var()
 *
 * Once variables are retrieved they are immediately removed from the global scope via the
 * unset calls below
 *
 * @param  string $key string of the key to retrieve.
 * @return string|array      the variable passed
 */
function ddb_get_template_vars( $key ) {

	/**
	 * Getting the template file that this function
	 * was called on
	 *
	 * @todo  look at the weight of debug_backtrace - from jorbin
	 */
	$file = debug_backtrace();
	$file = $file[0]['file'];

	/**
	 * Using dir/file-name as our $template_key to allow for duplicate template
	 * names in different directories
	 */
	$template_key = basename( dirname( $file ) ) . '/' . basename( $file, '.php' );

	/**
	 * Getting access to our global array
	 */
	global $ddb_template_part_vars;

	/**
	 * Checking that he requested key exists
	 */
	if ( array_key_exists( $template_key, $ddb_template_part_vars ) ) {

		/**
		 * Setting our return value
		 */
		$return = $ddb_template_part_vars[ $template_key ][ $key ];

		/**
		 * Removing our value to account for nested template parts
		 * that may have the same global array keys, i.e. we just got
		 * our variables, so we dont need this in the global scope anymore
		 */
		unset( $ddb_template_part_vars[ $template_key ][ $key ] );
		unset( $ddb_template_part_vars[ $template_key ] );

		/**
		 * Sending our variable/s back
		 */
		return $return;

	} else {
		/**
		 * @todo  consider is returning null is appropriate here, probably best to explicitly return null - from jorbin
		 */
	}
}

if ( defined( 'GET_TEMPLATE_WITH_VAR_DEBUG' ) && true === GET_TEMPLATE_WITH_VAR_DEBUG ) {
	add_action( 'get_footer', 'get_template_with_var_debug' );
}

/**
 * If GET_TEMPLATE_WITH_VAR_DEBUG is set to true, this function will show any unused
 * variables that are still in the global scope. In practice there should never be
 * any variables left in the global scope
 */
function get_template_with_var_debug() {
	?>

	<div>
		<?php
		global $ddb_template_part_vars;

		if ( ! empty( $ddb_template_part_vars ) ) {

			foreach ( $ddb_template_part_vars as $key => $var ) : ?>
				<div>
					<h2>Unused variable:</h2>
					<?php
					echo '<pre>';
					print_r( $var );
					echo '</pre>';
					?>
					in <?php echo esc_html( $key ); ?>
					<hr>
				</div> <!-- End debug div -->
			<?php
			endforeach;
		} ?>

	</div>
<?php }
